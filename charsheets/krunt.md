# Krunt
<div markdown="1" class="section">


<div markdown="1" class="colstwo">

## Power ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg)

Attacking, lifting, pushing, breaking, and sometimes climbing.
{: .smalltext}

## Agility ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg)

Defending, running, jumping, sneaking, hiding, dodging, swinging, and sometimes climbing
{: .smalltext}

## Brains ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg)

Intelligence, wisdom, personality, disguises, discernment, perspicacity, spells, clues, psychic abilities, languages, codes, history.
{: .smalltext}

## Health ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg)

Health Points, stamina, fatigue, carrying, breath holding, disease resistance, poison resistance.
{: .smalltext}

## Dare ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg) ![](d6outline.svg)

Daring, focus, luck, chutzpah, fate, karma.
{: .smalltext}

**Combat skills: ** +2h Combat related rolls. +1 Crit rolls. This character can divide up their attack dice between opponents.

**Equipment:** Sack, Healing herbs (6) [+2h to a *healing* check.], Lantern, flint & steel

**Doom:** Gambles

## Money ![](d6outline.svg) ![](d6outline.svg) ![](d6_tiny.svg) ![](d6_tiny.svg) ![](d6_tiny.svg) ![](d6_tiny.svg) ![](d6_tiny.svg) 

### Fist {: .breakbefore}

1. Black eye. +1 damage and possible quick hand action.
1. Uppercut knocks them flat.
1. Kidney shot. Fall over and actions are at -1d.
1. Disarmed. Weapon thrown 5d' away.
1. Uppercut. +2 damage.
1. Haymaker. +2 damage and knock them flat.
1. Eye strike. -1 attack until healed.
1. Knee damaged. Movement reduced by half. -2 **power** and **agility** reduced to 1.
1. Disarmed and weapon grabbed. Make a free attack with it now.
1. K.O. prone and make 3 **health** hits to awaken.
1. Neck strike. +3 damage, drop weapon, and lose a turn.
1. Chained combo. Keep rolling attacks until one fails or you fumble.
1. Pull their still beating heart from their chest and show it to them.

### Sword (+2d)

1. Gain +1 **Armor** agianst this opponent until next round.
1. Opponent cannot crit on you with their next attack.
1. Opponent loses their highest rolled attack die if they attack you.
1. +2 Damage to weapon arm. -2d attack.
1. Bleeding wound. +2 damage and 1 damage/round until bleeding is stopped.
1. Powerful strike. +3 damage and 3h *agility* check or be pushed back.
1. Skillful attack. Choice of grappled, pushed back on other effect.
1. Trip up. +4 damage and 4h *agility* check or prone.
1. Weapon arm removed and smacks another opponent.
1. Gory attack. +2d6 damage and other opponents may flee.
1. *You killed my father. Prepare to die* +d66 damage.
1. Speed demon. Make 1d6 more attacks against opponents in close range.
1. Blur of blades. All opponents in melee range take 2d6 damage.

</div><!--end col -->
</div><!--end page-->
</div><!--end section-->
